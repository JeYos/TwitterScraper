from argparse import ArgumentParser
import folium
import json


def get_parser():
    parser = ArgumentParser()
    parser.add_argument('--geojson')
    parser.add_argument('--map')
    return parser

parser=get_parser()

args=parser.parse_args()
    #	Read	tweet	collection	and	build	geo	data	structure
with open(args.tweets,	'r')as f:
        geo_data={"type":"FeatureCollection","features":[]}
        for	line in	f:
            tweet=json.loads(line)
            if	tweet['coordinates']:geo_json_feature={"type":"Feature","geometry":{"type":"Point",
                        "coordinates":tweet['coordinates'] ['coordinates']},
                        "properties":{"text":tweet['text'],
                        "created_at":tweet['created_at']}}
        geo_data['features'].append( geo_json_feature)

            #	Save	geo	data
        with open(args.geojson,	'w')as fout:
                 fout.write(json.dumps(geo_data,indent=4))



def make_map(geojson_file, map_file):
    tweet_map = folium.Map(location=[50, 5], zoom_start=5)
    geojson_layer = folium.GeoJson(open(geojson_file), name='geojson')
    geojson_layer.add_to(tweet_map)
    tweet_map.save(map_file)

parser = get_parser()
args = parser.parse_args()

make_map(args.geojson, args.map)