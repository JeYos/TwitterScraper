import sys
import json
from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

import pandas as pd
import numpy as np
import pickle
from ast import literal_eval
import tweepy
import csv
from TwitAuth import get_twitter_client
fname = 'stream_s_p_o_r_t.json'
all_dates = []
api=get_twitter_client()
key="OMATHL"
fieldnames = ['created_at','text']
csvFile = open(key + '.csv', 'w', encoding='utf8')
f = csv.DictWriter(csvFile, fieldnames=fieldnames)
f.writeheader()
for tweets in f:
    tweet = json.loads(tweets)
    f.write({'created_at': tweet["created_at"]})
    csvFile.close()
    print(key + "File Created")

        # all_dates.append(line.get('created_at'))

DF=pd.read_csv('jerusalem.csv')
for c  in DF:
   all_dates.append(c)
idx = pd.DatetimeIndex(all_dates)
ones = np.ones(
len(all_dates))  # the	actual	series	(at	series	of	1s	for	the	moment)
my_series	=	pd.Series(ones,	index=idx)

#	Resampling	/	bucketing	into	1-minute	buckets
per_minute	=	my_series.resample('1Min',	how='sum').fillna(0)
#	Plotting	the	series
fig,	ax	=	plt.subplots()
ax.grid(True)
ax.set_title("Tweet	Frequencies")
hours	=	mdates.MinuteLocator(interval=20)
date_formatter	=	mdates.DateFormatter('%H:%M')

datemin = datetime(2018, 5, 17, 6, 0)
datemax = datetime(2018, 5, 17, 23, 0)

ax.xaxis.set_major_locator(hours)
ax.xaxis.set_major_formatter(date_formatter)
ax.set_xlim(datemin, datemax)
max_freq = per_minute.max()
ax.set_ylim(0, max_freq)
ax.plot(per_minute.index, per_minute)

plt.savefig('tweet_time_series.png')
