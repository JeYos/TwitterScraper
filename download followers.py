import os
import sys
import json
import time
import math
from tweepy import Cursor
from TwitAuth import get_twitter_client

def	paginate(items,	n):
    for	i in range(0,len(items),	n):
      yield	items[i:i+n]
MAX_FRIENDS = 	15000

screen_name = "OMAthl"
client = get_twitter_client()
dirname = "C:/Users/user/PycharmProjects/Projet python VF/{}".format(screen_name)
max_pages = math.ceil(MAX_FRIENDS / 5000)
os.makedirs(dirname,mode=0o755,	exist_ok=True)
#	get	followers	for	a	given	user
fname="C:/Users/user/PycharmProjects/Projet python VF/{}/followers.jsonl".format(screen_name)
with open(fname,'w')as f:
    for	followers in Cursor(client.followers_ids,screen_name=screen_name).pages(max_pages):
        for	j in paginate(followers,	100):
            users=client.lookup_users(user_ids=j)
            for user in	users:
              f.write(json.dumps(user._json)+"\n")
              if len(followers) == 5000:
                  print( "More	results	available")
              #	get	user's	profile
#fname="C:/Users/user/PycharmProjects/Projet python VF/{}/user_profile.json".format(screen_name)
#with open(fname,	'w') as	fl:
  #  profile	=client.get_user(screen_name=screen_name)
  #  fl.write(json.dumps(profile._json))
#print("fini")
